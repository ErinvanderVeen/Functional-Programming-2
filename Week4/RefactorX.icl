implementation module RefactorX

import StdEnv

Start				= map eval [E1,E2,E3,E4,E5]

E1					= OP (LET "x" (OP (NR 42) MIN (NR 3)) (OP (VAR "x") DIV (NR 0))) PLUS (LET "y" (NR 6) (OP (VAR "y") MUL (VAR "y")))
E2					= LET "x" (NR 42) (OP (VAR "x") PLUS (LET "x" (NR 58) (VAR "x")))
E3					= LET "x" (NR 1) (LET "y" (NR 2) (LET "x" (NR 3) (NR 4)))
E4					= LET "x" (NR 1) (OP (VAR "x") PLUS (VAR "y"))
E5					= OP (LET "x" (NR 1) (VAR "x")) MUL (VAR "x")

::	Expr			= NR   Int
					| VAR  Name
					| LET  Name     Expr Expr
					| OP   Expr Operator Expr
::	Name			:== String
::	Operator		= PLUS | MIN | MUL | DIV
::	Val				= Result Int | Undef

// From slides
(<+) infixl 9 :: String a -> String | toString a
(<+) str a = str +++ toString a

instance toString Operator where
	toString PLUS = " + "
	toString MIN = " - "
	toString MUL = " * "
	toString DIV = " / "

instance toString Expr where
	toString (NR n) = toString n
	toString (VAR x) = x
	toString (LET n e1 e2) = "let " <+ n <+ " = " <+ e1 <+ " in " <+ e2
	toString (OP e1 op e2) = bracketString e1 <+ op <+ bracketString e2
	where
		bracketString :: Expr -> String
		bracketString (OP e1 op e2) = "( " <+ e1 <+ op <+ e2 <+ " )"
		bracketString (LET n e1 e2) = "( " <+ (LET n e1 e2) <+ " )"
		bracketString x = toString x // NR and VAR together

free :: Expr -> [Name]
free (NR _) = []
free (VAR x) = [x]
free (LET n _ e2) = [x \\ x <- free e2 | n <> x]
free (OP e1 _ e2) = removeDup (free e1 ++ free e2)

remove_unused_lets :: Expr -> Expr
remove_unused_lets (LET n e1 e2)
| isMember n (free e2) = (LET n (remove_unused_lets e1) (remove_unused_lets e2))
| otherwise = remove_unused_lets e2
remove_unused_lets (OP e1 op e2) = OP (remove_unused_lets e1) op (remove_unused_lets e2)
remove_unused_lets x = x

eval :: Expr -> Val
eval e
| not (isEmpty (free e)) = Undef
| otherwise = eval` (remove_unused_lets e) []
where
	eval` :: Expr [(Name, Val)] -> Val
	eval` (NR n) _ = Result n
	eval` (VAR x) vars = find_value x vars
		where
		find_value :: Name [(Name, Val)] -> Val
		find_value _ [] = Undef
		find_value x [(name, value):vars]
		| x == name = value
		| otherwise = find_value x vars
	eval` (LET n e1 e2) vars
	# var = (n, eval` e1 vars)
	= eval` e2 [var:vars]
	eval` (OP e1 op e2) vars = eval_op op (eval` e1 vars) (eval` e2 vars)
	where
		eval_op :: Operator Val Val -> Val
		eval_op _ Undef _ = Undef
		eval_op _ _ Undef = Undef
		eval_op PLUS (Result r1) (Result r2) = Result (r1 + r2)
		eval_op MIN (Result r1) (Result r2) = Result (r1 - r2)
		eval_op MUL (Result r1) (Result r2) = Result (r1 * r2)
		eval_op DIV _ (Result 0) = Undef
		eval_op DIV (Result r1) (Result r2) = Result (r1 / r2)
