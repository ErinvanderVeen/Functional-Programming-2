definition module RefactorX

import StdEnv

::	Expr			= NR   Int
					| VAR  Name
					| LET  Name     Expr Expr
					| OP   Expr Operator Expr
::	Name			:== String
::	Operator		= PLUS | MIN | MUL | DIV
::	Val				= Result Int | Undef

from StdClass import class toString

instance toString Expr
free				:: Expr -> [Name]
remove_unused_lets	:: Expr -> Expr
eval				:: Expr -> Val
