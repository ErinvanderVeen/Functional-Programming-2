definition module RefactorXX

from StdClass import class toString

:: Expr        = NR   Int
               | VAR  Name
               | LET  Name     Expr Expr
               | OP   Expr Operator Expr
:: Name        :== String
:: Operator    = PLUS | MIN | MUL | DIV
:: Val a       = Result a | Undef

class fail          c :: c a
class return        c :: a -> c a
class (>>=) infix 0 c :: (c a) (a -> c b) -> c b
class Monad         c | return, >>= c
class MonadFail     c | Monad, fail c

instance fail   [], Val
instance return [], Val
instance >>=    [], Val

instance toString Expr
free                :: Expr -> [Name]
remove_unused_lets  :: Expr -> Expr
eval                :: Expr -> c Int | MonadFail c
