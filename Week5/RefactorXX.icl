implementation module RefactorXX

import StdClass, StdInt, StdList, StdOverloaded, StdString

/*
E1 = (let x = 42 - 3 in x / 0) + (let y = 6 in y * y)
E2 = let x = 42 in x + (let x = 58 in x)
E3 = let x = 1 in let y = 2 in let x = 3 in 4
E4 = let x = 1 in x + y
E5 = (let x = 1 in x) * x
E6 = let x = 5 in let y = x in 0
*/

E1 = (OP (LET "x" (OP (NR 42) MIN (NR 3)) (OP (VAR "x") DIV (NR 0))) PLUS (LET "y" (NR 6) (OP (VAR "y") MUL (VAR "y"))))
E2 = (LET "x" (NR 42) (OP (VAR "x") PLUS (LET "x" (NR 58) (VAR "x"))))
E3 = (LET "x" (NR 1) (LET "y" (NR 2) (LET "x" (NR 3) (NR 4))))
E4 = (LET "x" (NR 1) (OP (VAR "x") PLUS (VAR "y")))
E5 = (OP (LET "x" (NR 1) (VAR "x")) MUL (VAR "x"))
E6 = (LET "x" (NR 5) (LET "y" (VAR "x") (NR 0)))

:: Expr        = NR   Int
               | VAR  Name
               | LET  Name     Expr Expr
               | OP   Expr Operator Expr
:: Name        :== String
:: Operator    = PLUS | MIN | MUL | DIV
:: Val a       = Result a | Undef

class fail          c :: c a
class return        c :: a -> c a
class (>>=) infix 0 c :: (c a) (a -> c b) -> c b
class Monad         c | return, >>= c
class MonadFail     c | Monad, fail c

instance fail Val where
    fail = Undef

instance return Val where
    return x = Result x

instance >>= Val where
    (>>=) Undef _ = fail
    (>>=) (Result x) next = next x

instance fail [] where
    fail = []

instance return [] where
    return x = [x]

instance >>= [] where
    (>>=) [] _ = fail
    (>>=) x next = flatten (map next x)

// From slides
(<+) infixl 9 :: String a -> String | toString a
(<+) str a = str +++ toString a

instance toString Operator where
    toString PLUS = " + "
    toString MIN = " - "
    toString MUL = " * "
    toString DIV = " / "

instance toString Expr where
    toString (NR n) = toString n
    toString (VAR x) = x
    toString (LET n e1 e2) = "let " <+ n <+ " = " <+ e1 <+ " in " <+ e2
    toString (OP e1 op e2) = bracketString e1 <+ op <+ bracketString e2
    where
        bracketString :: Expr -> String
        bracketString (OP e1 op e2) = "( " <+ e1 <+ op <+ e2 <+ " )"
        bracketString (LET n e1 e2) = "( " <+ (LET n e1 e2) <+ " )"
        bracketString x = toString x // NR and VAR together

free :: Expr -> [Name]
free (NR _) = []
free (VAR x) = [x]
free (LET n e1 e2) = filter ((<>) n) (free e1 ++ free e2)
free (OP e1 _ e2) = removeDup (free e1 ++ free e2)

remove_unused_lets :: Expr -> Expr
remove_unused_lets (LET n e1 e2)
| isMember n (free (remove_unused_lets e2)) = (LET n (remove_unused_lets e1) (remove_unused_lets e2))
| otherwise = remove_unused_lets e2
remove_unused_lets (OP e1 op e2) = OP (remove_unused_lets e1) op (remove_unused_lets e2)
remove_unused_lets x = x

eval :: Expr -> c Int | MonadFail c
eval e
| not (isEmpty (free e)) = fail
| otherwise = eval` (remove_unused_lets e) []
where
    eval` :: Expr [(Name, c Int)] -> c Int | MonadFail c
    eval` (NR n) _ = return n
    eval` (VAR x) vars = find_value x vars
    where
        find_value :: Name [(Name, c Int)] -> c Int | MonadFail c
        find_value _ [] = fail
        find_value x [(name, value):vars]
        | x == name = value
        | otherwise = find_value x vars
    eval` (LET n e1 e2) vars
    # var = (n, eval` e1 vars)
    = eval` e2 [var:vars]
    eval` (OP e1 op e2) vars = eval_op op (eval` e1 vars) (eval` e2 vars)
    where
        eval_op :: Operator (c Int) (c Int) -> c Int | MonadFail c
        eval_op PLUS r1 r2 = r1 >>= \val1 -> r2 >>= \val2 -> return (val1 + val2)
        eval_op MIN r1 r2 = r1 >>= \val1 -> r2 >>= \val2 -> return (val1 - val2)
        eval_op MUL r1 r2 = r1 >>= \val1 -> r2 >>= \val2 -> return (val1 * val2)
        eval_op DIV r1 r2 = r1 >>= \val1 -> r2 >>= \val2 -> (if (val2 == 0) fail (return (val1 / val2)))
        eval_op _ _ _ = fail

// Test lijst instances MonadFail
//Start :: [[Int]]
//Start = map eval [E1, E2, E3, E4, E5, E6]

// Test Val instances MonadFail
Start :: [Val Int]
Start = map eval [E1, E2, E3, E4, E5, E6]
