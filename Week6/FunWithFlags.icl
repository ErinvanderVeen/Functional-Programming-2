implementation module FunWithFlags

import QA, StdEnv

//	A number of flags, see http://nl.wikipedia.org/wiki/Lijst_van_vlaggen_van_Europa

fun_with_flags :: [QA]
fun_with_flags
	= [(azerbaijan, "Azerbaijan"),
	(european_union, "European Union"),
	(bosnia_and_herzegovina, "Bosnia and Herzegovina"),
	(greece, "Greece"),
	(turkey, "Turkey"),
	(switzerland, "Switzerland"),
	(fit (px 300.0) (px 200.0) (stripedflag [toSVGColor {r = 173, g = 28, b = 40}, toSVGColor "white", toSVGColor {r = 33, g = 70, b = 139}]), "Netherlands"),
	(fit (px 300.0) (px 187.5) (stripedflag [toSVGColor {r = 255, g = 255, b = 255}, toSVGColor {r = 212, g = 33, b = 61}]), "Poland"),
	(fit (px 300.0) (px 200.0) (stripedflag [toSVGColor {r = 212, g = 33, b = 61}, toSVGColor {r = 255, g = 255, b = 255}]), "Monaco"),
	(fit (px 300.0) (px 180.0) (stripedflag [toSVGColor "black", toSVGColor {r = 255, g = 0, b = 0}, toSVGColor {r = 255, g = 204, b = 0}]), "Germany"),
	(fit (px 300.0) (px 200.0) (columnflag [toSVGColor {r = 0, g = 85, b = 164}, toSVGColor "white", toSVGColor {r = 239, g = 65, b = 53}]), "France"),
	(fit (px 300.0) (px 200.0) (columnflag [toSVGColor {r = 0, g = 146, b = 70}, toSVGColor "white", toSVGColor {r = 206, g = 43, b = 55}]), "Italy"),
	(fit (px 300.0) (px 150.0) (columnflag [toSVGColor {r = 22, g = 155, b = 98}, toSVGColor "white", toSVGColor {r = 255, g = 136, b = 62}]), "Ireland"),
	(kruisvlag (toSVGColor {r = 198, g = 12, b = 48}) (toSVGColor "white") (toSVGColor "white"), "Denmark"),
	(kruisvlag (toSVGColor {r = 0, g = 127, b = 229}) (toSVGColor {r = 255, g = 204, b = 0}) (toSVGColor {r = 255, g = 204, b = 0}), "Sweden")]

kruisvlag :: SVGColor SVGColor SVGColor -> Image m // background, outercross, innercross
kruisvlag achtergrond buitensteKruis binnensteKruis = overlay (repeatn 2 (AtMiddleX, AtMiddleY)) (repeatn 2 (px 0.0, px 0.0)) [fitx (px 220.0) (kruis buitensteKruis 3.0 6.0), fitx (px 220.0) (kruis binnensteKruis 2.0 7.0)] (Just ((rect (px 220.0) (px 160.0)) <@< {fill = achtergrond} <@< {strokewidth = px 0.0}))
where
	kruis :: SVGColor Real Real -> Image m
	kruis color size offset = overlay (repeatn 2 (AtLeft, AtMiddleY)) [(px offset, px 0.0), (px 0.0, px 0.0)] [(rect (px size) (px 16.0)) <@< {fill = color} <@< {strokewidth = px 0.0}, (rect (px 22.0) (px size)) <@< {fill = color} <@< {strokewidth = px 0.0}] (Just ((rect (px 22.0) (px 16.0)) <@< {fill = toSVGColor "none"} <@< {strokewidth = px 0.0}))

european_union :: Image m
european_union = overlay (repeatn 12 (AtMiddleX, AtMiddleY)) (locations 12) (repeatn 12 (fity (px 22.22) (star 5 (toSVGColor {r = 255, g = 204, b = 0})))) (Just ((rect (px 300.0) (px 200.0)) <@< {fill = toSVGColor {r = 0, g = 51, b = 153}}))
where
	locations :: Int -> [ImageOffset]
	locations nr = map (\x -> (px (66.6 * (sin (toReal x * 2.0 * pi / (toReal nr)))), px (-66.6 * (cos (toReal x * 2.0 * pi / (toReal nr)))))) [1 .. nr]

azerbaijan :: Image m
azerbaijan = overlay (repeatn 3 (AtMiddleX, AtMiddleY)) [(px 193.33, px 100.0), (px 200.0, px 100.0), (px 225.0, px 100.0)] [(circle (px 60.0) <@< {fill = toSVGColor "white"} <@< {strokewidth = px 0.0}), (circle (px 50.0) <@< {fill = toSVGColor {r = 239, g = 51, b = 64}} <@< {strokewidth = px 0.0}), fity (px 33.33) (star 8 (toSVGColor "white"))] (Just (fit (px 400.0) (px 200.0) (stripedflag [toSVGColor {r = 0, g = 181, b = 226}, toSVGColor {r = 239, g = 51, b = 64}, toSVGColor{r = 80, g = 158, b = 47}])))

bosnia_and_herzegovina :: Image m
bosnia_and_herzegovina = overlay (repeatn 2 (AtMiddleX, AtMiddleY)) [(px 0.0, px 0.0),(px -20.0, px 0.0)] [fity (px 200.0) driehoek, fity (px 200.0) stars] (Just (rect (px 400.0) (px 200.0) <@< {fill = toSVGColor {r = 0, g = 51, b = 153}}))
where
	driehoek :: Image m
	driehoek = (polygon Nothing [(px 0.0, px 0.0), (px 1.0, px 0.0), (px 1.0, px 1.0)]) <@< {fill = toSVGColor "yellow"} <@< {strokewidth = px 0.0}

	stars :: Image m
	stars = overlay (repeatn 9 (AtMiddleX, AtMiddleY)) (star_location 9) (repeatn 9 (fitx (px 1.5) (star 5 (toSVGColor "white")))) (Just (rect (px 9.0) (px 9.0) <@< {fill = toSVGColor "none"} <@< {strokewidth = px 0.0}))
	where
		star_location :: Int -> [ImageOffset]
		star_location n = map (\x -> (px (1.1 * toReal x - 0.5 * toReal n), px (1.1 * toReal x - 0.5 * toReal n))) [0 .. n-1]

greece :: Image m
greece = overlay [(AtLeft, AtTop), (AtLeft, AtTop)] [(px 0.0, px 0.0), (px 0.0, px 0.0)] [(fity (px 111.11) ((rect (px 1.0) (px 1.0)) <@< {strokewidth = px 0.0} <@< {fill = toSVGColor {r = 36, g = 64, b = 242}})), cross (toSVGColor "white") 111.11 112.0 22.22] (Just (fit (px 300.0) (px 200.0) (stripedflag (interleave (repeatn 5 (toSVGColor {r = 36, g = 64, b = 242})) (repeatn 4 (toSVGColor "white"))))))
where
	interleave :: [SVGColor] [SVGColor] -> [SVGColor]
	interleave xs [] = xs
	interleave [] ys = ys
	interleave [x:xs] [y:ys] = [x] ++ [y] ++ (interleave xs ys)

cross :: SVGColor Real Real Real -> Image m
cross color width height crosswidth = overlay [(AtMiddleX, AtMiddleY)] [] [rect (px crosswidth) (px height) <@< {fill = color} <@< {strokewidth = px 0.0}] (Just (overlay [(AtMiddleX, AtMiddleY)] [] [rect (px width) (px crosswidth) <@< {fill = color} <@< {strokewidth = px 0.0}] (Just ((rect (px width) (px height)) <@< {fill = toSVGColor "none"} <@< {strokewidth = px 0.0})) ))

turkey :: Image m
turkey = overlay [(AtMiddleX, AtMiddleY), (AtMiddleX, AtMiddleY), (AtMiddleX, AtMiddleY)] [(px -50.0, px 0.0), (px -37.5, px 0.0), (px 10.0, px 5.0)] [circle (px 100.0) <@< {fill = toSVGColor "white"} <@< {strokewidth = px 0.0}, circle (px 80.0) <@< {fill = toSVGColor {r = 227, g = 10, b = 23}} <@< {strokewidth = px 0.0}, rotate (deg -20.0) (fity (px 50.0) (star 5 (toSVGColor "white")))] (Just (rect (px 300.0) (px 200.0) <@< {fill = toSVGColor {r = 227, g = 10, b = 23}} <@< {strokewidth = px 0.0}))

switzerland :: Image m
switzerland = overlay [(AtMiddleX, AtMiddleY)] [] [cross (toSVGColor "white") 120.0 120.0 40.0] (Just (rect (px 200.0) (px 200.0) <@< {fill = toSVGColor {r = 255, g = 0, b = 0}} <@< {strokewidth = px 0.0}))

star :: Int SVGColor -> Image m
star nr color = polygon Nothing (points nr) <@< {fill = color} <@< {strokewidth = px 0.0}
where
	points :: Int -> [ImageOffset]
	points nr = map (\x -> (px (sin (toReal x * 2.0 * pi / (toReal nr))), px (-1.0 * (cos (toReal x * 2.0 * pi / (toReal nr)))))) (order nr)
	where
		order :: Int -> [Int]
		order x = order` (\y -> y + (toInt ((x-1)/2))) (repeatn x 1)
		where
			order` :: (Int -> Int) [Int] -> [Int]
			order` _ [] = []
			order` f [x:xs] = [x : order` f (map f xs)]

// Creates an Image which is n * 1 pixels in size where n is the amount of colors. We do this so that flags can be rescaled to the correct aspect ratio later.
stripedflag :: [SVGColor] -> Image m
stripedflag colors = above [] [] [image <@< {fill = color} \\ image <- (repeatn (length colors) ((rect (px 1.0) (px 1.0)) <@< {strokewidth = px 0.0})) & color <- colors] Nothing

columnflag :: [SVGColor] -> Image m
columnflag colors = beside [] [] [image <@< {fill = color} \\ image <- (repeatn (length colors) ((rect (px 1.0) (px 1.0)) <@< {strokewidth = px 0.0})) & color <- colors] Nothing
