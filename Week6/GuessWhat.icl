module GuessWhat

import iTasks							// de algemene iTask API
import iTasks.API.Extensions.SVG.SVGlet	// specialiseer task editors
from   Data.List import zipWith

import FunWithFlags						// if you go for European flags

nr_of_qas :== 6							// the number of actual images that need to be guessed

Start :: *World -> *World
Start world
	= startEngine [publish "/" (WebApp [])
	                           (\_ -> play fun_with_flags)
	              ] world

play :: [QA] -> Task String
play []
	= viewInformation "missing queries" [] "No queries are available"
play qas
	=             sequence "throw dice" (repeatn (nr_of_qas + length qas) (get randomInt))
	  >>= \nrs -> let (nrs1,nrs2)	= splitAt nr_of_qas nrs
	                  shuffled_qas	= shuffle nrs2 qas
	                  (qs,as)		= unzip (take nr_of_qas shuffled_qas)
	                  sas			= shuffle nrs1 as
	               in keep_guessing qs as sas

keep_guessing :: [Image ()] [String] [String] -> Task String
keep_guessing qs as sas
	=             allTasks` [guess i q sas \\ q <- qs & i <- [1 ..]]
	  >>* [ OnAction (Action "Check" []) (hasValue (check_answers qs as sas))
	      , OnAction ActionQuit          (always   (return "Goodbye"))
	      ] 

check_answers :: [Image ()] [String] [String] [String] -> Task String
check_answers qs as sas nas
| ok == nr_of_qas
	= viewInformation "Tada!" [] "Congratulations! All correct!"
| otherwise
	= (viewInformation "Ouch!" [] ("Too bad: there are " <+++ nr_of_qas - ok <+++ " mistakes.")
	      ||-
	   allTasks [  ((show_image i q <<@ ArrangeHorizontal)
	                    ||-
	                (viewInformation "isn't" [] a <<@ ArrangeHorizontal)
	               ) <<@ ArrangeHorizontal
	            \\ wrong <- zipWith (<>) as nas
	             & q     <- qs
	             & a     <- nas
	             & i     <- [1 ..]
	             | wrong
	            ]
	  ) >>* [ OnAction (Action "Try again" []) (always (keep_guessing qs as sas))
	        , OnAction ActionQuit              (always (return "Goodbye"))
	        ]
where
	ok = length [() \\ a <- as & b <- nas | a == b]

show_image :: Int (Image ()) -> Task ()
show_image i q = viewInformation ("image " <+++ i) [imageView (\_ -> ()) (\_ _ -> q) (\_ _ -> Nothing)] ()
where
	const2 a _ _   = a
	const3 a _ _ _ = a

guess :: Int (Image ()) [String] -> Task String
guess i q sas
	= ( (show_image i q <<@ ArrangeHorizontal)
           ||-
        (enterChoice "is:" [ChooseWith (ChooseFromComboBox id)] sas <<@ ArrangeHorizontal)
      ) <<@ ArrangeHorizontal

shuffle :: [Int] [a] -> [a]
shuffle rnrs as
	= fst (unzip (sortBy (\(_,r1) (_,r2) -> r1 < r2) (zip2 as rnrs)))

allTasks` :: [Task a] -> Task [a] | iTask a
allTasks` ts
| no_ts < 4	= allTasks ts
| otherwise	= allTasks [allTasks (ts%(i*per_row,(i+1)*per_row-1)) <<@ ArrangeHorizontal \\ i <- [0 .. no_rows-1]] @ flatten
where
	no_ts	= length ts
	no_rows	= toInt (sqrt (toReal no_ts))
	per_row	= no_ts / no_rows + if (no_ts rem no_rows == 0) 0 1
