definition module SimpleFileIO

import StdMaybe, StdEnv

// 1.
readFile	:: String                 *env -> (Maybe String,  *env) | FileSystem env

// 2.
readLines	:: String                 *env -> (Maybe [String],*env) | FileSystem env

