// Erin van der Veen - s4431200
// Oussama Danba - s4435591
module Galgje

import StdEnv, SimpleFileIO, RandomGetallen

kiesWoord :: *env -> (Maybe String, *env) | FileSystem env
kiesWoord world
# (str, world) = readLines "woorden" world
| isNothing str = (Nothing, world)
# (seed, world) = getNewRandomSeed world
= (Just (skipNewline ((shuffle (fromJust str) seed) !! 0)), world)

skipNewline :: String -> String
skipNewline str = if (size str > 0 && str.[size str-1] == '\n') (str % (0, size str-2)) str

printResultaat :: Bool *File -> *File
printResultaat res io
| res = (io <<< "\nJe hebt gewonnen! Gefeliciteerd!")
| otherwise  = (io <<< "\nHelaas, je hebt verloren.")

printGalg :: Int *File *env -> (*File, *env) | FileSystem env
printGalg n io world
# (str, world) = readFile ("galgen/" +++ fromInt n) world
| isNothing str = abort ("Kon bestand voor galg " +++ fromInt n +++ " niet lezen.")
# str = (fromJust str)
= (io <<< str, world)

vervangGok :: [Char] [Char] Char -> [Char]
vervangGok [] [] gok = []
vervangGok [w : ws] [t : ts] gok
| gok == w = [w : vervangGok ws ts gok]
| otherwise = [t : vervangGok ws ts gok]

speelRonde :: Int [Char] [Char] *env *File -> (Bool, *env, *File) | FileSystem env
speelRonde hp woord totNu world io
| not (isMember '_' totNu) = (True, world, io)
| hp == 0 = (False, world, io)
# (gok, io) = freadline io
# gok = skipNewline gok
| size gok > 1 && gok == toString woord = (True, world, io)
# gok = (fromString gok)!!0
# totNu = vervangGok woord totNu gok
# match = isMember gok woord
# io = if match (io <<< "Goed geraden!\n") (io <<< "Niet goed geraden.\n")
# hp = if match (hp) (hp-1)
# (io, world) = printGalg (hp) io world
# io = (io <<< ("Je hebt nog " +++ toString hp +++ " kansen.\n"))
# io = (io <<< (toString totNu) <<< "\n")
# io = (io <<< "Vul je letter in: ")
| otherwise = speelRonde (hp) woord totNu world io

Start :: *World -> *World
Start world
# (woord, world) = kiesWoord world
| isNothing woord = abort "Kon geen woord uitlezen."
# (Just woord) = woord
# (io, world) = stdio world
# woord = fromString woord
# totNu = repeatn (length woord) '_'
# io = (io <<< (toString totNu) <<< "\n")
# io = (io <<< "Vul je letter in:")
# (gewonnen, world, io) = speelRonde 8 woord totNu world io
# io = printResultaat gewonnen io
# (ok,world) = fclose io world
| not ok = abort "Kon stdio niet sluiten"
| otherwise = world

