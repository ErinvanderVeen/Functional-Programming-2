implementation module SimpleFileIO

import StdMaybe, StdEnv

// 1.
readFile :: String *env -> (Maybe String,*env) | FileSystem env
readFile path world
# (ls, world) = readLines path world
| isNothing ls = (Nothing, world)
| otherwise = (Just (foldl (+++) "" (fromJust ls)), world)

// 2.
readLines :: String *env -> (Maybe [String],*env) | FileSystem env
readLines str env
# (ok, file, env) = sfopen str FReadText env
| not ok = (Nothing, env)
# (str, file) = readLines` file
| otherwise = (Just str, env)
where
    readLines` :: File -> ([String], File)
    readLines` file
    # end = sfend file
    | end = ([], file)
    # (l, file) = sfreadline file
    # (ls, file) = readLines` file
    | otherwise = ([l : ls], file)

