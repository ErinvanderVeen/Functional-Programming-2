implementation module RandomGetallen

import StdEnv, Random

Start :: *World -> ([Int],*World)
Start world
# (rs,world)	= getNewRandomSeed world
= (shuffle [1 .. 10] rs, world)

random_n :: Int RandomSeed -> ([Int],RandomSeed)
random_n n rs = seqList (repeatn n random) rs

random_inf :: RandomSeed -> [Int]
random_inf rs = iterateSt (random) rs

iterateSt :: (s -> (a,s)) s -> [a]
iterateSt f rs
# (a, s) = f rs
= [a : iterateSt f s]

shuffle :: [a] RandomSeed -> [a]
shuffle [] _ = []
shuffle xs rs
# (rnd, rs) = random rs
# rnd = rnd rem (length xs)
= [xs!!rnd : shuffle (removeAt rnd xs) rs]

