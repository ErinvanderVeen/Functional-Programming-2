implementation module StdDynSet

import StdEnv
import StdDynamic

class Set a | TC, ==, toString a

// First element the tuple simply stores the dynamic.
// The second element stores a function to test for equalness.
// The third element is a string which is used for toString.
:: Set = Set [(Dynamic, Dynamic -> Bool, String)]

instance zero Set
where zero = Set []

instance toString Set
where
	toString (Set a) = "{" +++ printElements a +++ "}"
	where
		printElements :: [(Dynamic, Dynamic -> Bool, String)] -> String
		printElements [] = ""
		printElements [(_, _, a)] = a
		printElements [(_, _, a):as] = a +++ "," +++ printElements as

instance == Set
where == a b = isSubset a b && isSubset b a

// By storing the dynEqual function here we can avoid problems
// when trying to compare two dynamics since we no longer compare
// two dynamics but a value to a dynamic.
toSet :: a -> Set | Set a
toSet a = Set [(dynamic a, \f = dynEqual a f, toString a)]

nrOfElts :: Set -> Int
nrOfElts (Set a) = length a

isEmptySet :: Set -> Bool
isEmptySet a = (nrOfElts a) == 0

dynEqual :: t Dynamic -> Bool | Set t
dynEqual a (x :: t^) = a == x
dynEqual _ _ = False

memberOfSet :: a Set -> Bool | Set a
memberOfSet _ (Set []) = False
memberOfSet a (Set [(x, _, _):xs]) = dynEqual a x || memberOfSet a (Set xs)

dynamicMemberOfSet :: Dynamic Set -> Bool
dynamicMemberOfSet _ (Set []) = False
dynamicMemberOfSet a (Set [(_, f, _):xs]) = f a || dynamicMemberOfSet a (Set xs)

isSubset :: Set Set -> Bool
isSubset a b = nrOfElts b - nrOfElts a == nrOfElts (without b a)

isStrictSubset :: Set Set -> Bool
isStrictSubset a b = isSubset a b && nrOfElts a < nrOfElts b

union :: Set Set -> Set
union a b = Set (fromSet a ++ fromSet (without b a))
where
	fromSet :: Set -> [(Dynamic, Dynamic -> Bool, String)]
	fromSet (Set x) = x

intersection :: Set Set -> Set
intersection (Set as) b = Set [(a, f, st) \\ (a, f, st) <- as | dynamicMemberOfSet a b]

without :: Set Set -> Set
without (Set as) b = Set [(a, f, st) \\ (a, f, st) <- as | not (dynamicMemberOfSet a b)]
