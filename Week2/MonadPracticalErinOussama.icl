// Oussama Danba - s4435591
// Erin van der Veen - s4431200

module MonadPracticalErinOussama
import StdEnv

/// §1: The Monad Class and its Laws ///////////////////////////////////////////

class Monad m where
  pure           :: a -> m a
  (>>=) infixl 1 :: (m a) (a -> m b) -> m b

/// Left identity:
///   pure a >>= \x -> f x == f a
/// Right identity:
///   m >>= \x -> pure x == m
/// Associativity:
///   (m >>= \x -> f x) >>= \y -> g y == m >>= (\x -> (f x >>= \y -> g y))



/// §2: The Maybe monad ////////////////////////////////////////////////////////

:: Maybe a = Nothing | Just a

instance Monad Maybe where
  (>>=) Nothing  _    = Nothing
  (>>=) (Just x) next = next x

  pure x = Just x

/// §2.1: Safe division

/// §2.1.1

/// a
/// divide 42.0 3.0
/// -> Just (42.0 / 3.0)
/// -> Just 14.0

/// b
/// divide 42.0 0.0
/// -> Nothing

/// c
/// (divide 42.0 3.0) * 2.5
/// -> (Just (42.0 / 3.0)) * 2.5
/// -> (Just 14.0) * 2.5
/// -> Cannot unify types: Real Maybe Real


/// §2.1.2

/// Start = (divide 42.0 3.0) >>= \x -> pure (x * 2.5)

divide :: Real Real -> Maybe Real
divide x y
  | y == 0.0  = Nothing
  | otherwise = Just (x / y)

/// §2.2: Music facts

/// §2.2.3

/// a
/// Start = bestAlbum "Best singer"
/// -> lookup "Best singer" allFacts >>= \answer1 -> lookup answer1 allFacts >>= \answer2 -> lookup answer2 allFacts
/// -> Just "Ronnie James Dio" >>= \answer1 -> lookup answer1 allFacts >>= \answer2 -> lookup answer2 allFacts
/// -> lookup "Ronnie James Dio" allFacts >>= \answer2 -> lookup answer2 allFacts
/// -> Just "Dio" >>= \answer2 -> lookup answer2 allFacts
/// -> lookup "Dio" allFacts
/// -> Just "Holy Diver"

/// b
/// Start = bestAlbum "Ronnie James Dio"
/// -> lookup "Ronnie James Dio" allFacts >>= \answer1 -> lookup answer1 allFacts >>= \answer2 -> lookup answer2 allFacts
/// -> Just "Dio" >>= \answer1 -> lookup answer1 allFacts >>= \answer2 -> lookup answer2 allFacts
/// -> lookup "Dio" allFacts >>= \answer2 -> lookup answer2 allFacts
/// -> Just "Holy Diver" >>= \answer2 -> lookup answer2 allFacts
/// -> lookup "Holy Diver" allFacts
/// -> Nothing

allFacts :: [(String, String)]
allFacts =  [ ("Best singer",      "Ronnie James Dio")
            , ("Ronnie James Dio", "Dio")
            , ("Dio",              "Holy Diver") ]

lookup :: String [(String, String)] -> Maybe String
lookup key []   = Nothing
lookup key [(key`, value) : rest]
  | key` == key = Just value
  | otherwise   = lookup key rest

bestAlbum :: String -> Maybe String
bestAlbum category = lookup category allFacts
     >>= \answer1 -> lookup answer1  allFacts
     >>= \answer2 -> lookup answer2  allFacts

/// §2.3: Fitting the laws

/// §2.3.4
/// We must prove that: (m >>= \x -> f x) >>= \y -> g y == m >>= (\x -> (f x >>= \y -> g y))
/// In case one of the functions evaluates to Nothing, the entire thing will also evaluate to Nothing. Thus proving associativity in the case one of the functions is Nothing. As a result we will ignore it in the rest of the proof.
/// (m >>= \x -> f x) >>= \y -> g y
/// = (m >>= f) >>= g
/// = g (m >>= f)
/// = g (f m)
/// = (g f) m
/// = (f >>= g) m
/// = m >>= (f >>= g)
/// = m >>= (\x -> (f x >>= \y -> g y)

/// §3: The Either monad ///////////////////////////////////////////////////////

/// §3.1
:: Either e a = Left e | Right a

instance Monad (Either e) where
  (>>=) (Left e) _ = Left e
  (>>=) (Right a) next = next a

  pure x = Right x

/// §3.2
/// Start = map parsePerson [ ("Alice", "37", "f"), ("Bob", "2.5", "m"), ("Carol", "18", "o"), ("Dave", "", "f") ]
/// -> [ parsePerson ("Alice", "37", "f"), parsePerson ("Bob", "2.5", "m"), parsePerson ("Carol", "18", "o"), parsePerson ("Dave", "", "f") ]
/// -> [ case parseAge "37" of
///         Left e    -> Left e
///         Right age ->
///           case parseGender "f" of
///             Left e       -> Left e
///             Right gender -> Right {name = "Alice", age = age, gender = gender}
///               , parsePerson ("Bob", "2.5", "m"), parsePerson ("Carol", "18", "o"), parsePerson ("Dave", "", "f") ]
/// -> [ case Right 37 of
///         Left e    -> Left e
///         Right 37 ->
///           case Right Female of
///             Left e       -> Left e
///             Right Female -> Right {name = "Alice", age = 37, gender = Female}
///               , parsePerson ("Bob", "2.5", "m"), parsePerson ("Carol", "18", "o"), parsePerson ("Dave", "", "f") ]
/// -> [ Right {name = "Alice", age = 37, gender = Female}, parsePerson ("Bob", "2.5", "m"), parsePerson ("Carol", "18", "o"), parsePerson ("Dave", "", "f") ]
/// ...
/// -> [ Right {name = "Alice", age = 37, gender = Female}, Left "2.5 is not a valid age", Left "Unknown gender", Left "Nothing to parse as age"]

/// §3.3
parsePerson :: (String, String, String) -> Either Error Person
parsePerson (n, a, g) = parseAge a
  >>= \age -> parseGender g
  >>= \gender -> pure {name = n, age = age, gender = gender}

/// Person parser

:: Person  = {name :: String, age :: Int, gender :: Gender}
:: Gender  = Male | Female
:: Error :== String

parseAge :: String -> Either Error Int
parseAge ""              = Left "Nothing to parse as age"
parseAge str
  # x = toInt str
  | x == 0 && str <> "0" = Left (str +++ " is not a valid age")
  | otherwise            = Right x

parseGender :: String -> Either Error Gender
parseGender "m" = Right Male
parseGender "f" = Right Female
parseGender  _  = Left "Unknown gender"


/// §4: The List monad /////////////////////////////////////////////////////////

/// §4.1
/// in3 :: Position -> [Position]
/// in3 pos = flatten (map moveKnight (flatten (map moveKnight (flatten (map moveKnight [pos])))))

/// §4.2
/// We can definitely see a recurring pattern in our in3 function. (flatten + map for
/// every iteration)

instance Monad [] where
  (>>=) [] _ = []
  (>>=) a next = flatten (map next a)

  pure x = [x]

/// §4.3
in3 :: Position -> [Position]
in3 pos = moveKnight pos >>= \pos1 -> moveKnight pos1 >>= \pos2 -> moveKnight pos2 >>= \pos3 -> pure pos3

/// Chess moves

:: Position :== (Int, Int)

moveKnight :: Position -> [Position]
moveKnight (x, y) = filter onBoard
  [ (x+2, y-1), (x+2, y+1), (x-2, y-1), (x-2, y+1)
  , (x+1, y-2), (x+1, y+2), (x-1, y-2), (x-1, y+2)
  ]

onBoard :: Position -> Bool
onBoard (x,y) = 0 < x && x < 9 &&
                0 < y && y < 9

/// §5: The State monad ////////////////////////////////////////////////////////

/// §5.1
instance Monad (State s) where
  (>>=) st next = \s -> let (a, s1) = st s in (next a) st s1

  pure x = \s -> (x, s)

/// Random Numbers

:: RandomSeed :== Int

newRandomSeed :: !*World -> *(!RandomSeed, !*World)
newRandomSeed world = (123456789, world) //time world

random :: !RandomSeed -> (!Int, !RandomSeed)
random seed = (newSeed, newSeed)
where
  newSeed = abs ((a * seed + c) rem m)
  m = 2147483648
  a = 6364136223846793005
  c = 1442695040888963407

:: State s a = State (s -> (a, s))

eval :: (State s a) s -> a
eval (State f) s
  # (x, _) = f s
  = x

state :: (s -> (a, s)) -> State s a
state f = State f

/// Yahtzee
roll :: RandomSeed -> (Int, RandomSeed)
roll s
  # (x, seed) = random s
  = (x rem 6 + 1, seed)

isLargeStraight :: [Int] -> Bool
isLargeStraight dices = sorted == [1, 2, 3, 4, 5] ||
                        sorted == [2, 3, 4, 5, 6]
                        where sorted = sort dices

rollDices :: RandomSeed -> (Bool, RandomSeed)
rollDices seed
  # (d1, seed) = roll seed
  # (d2, seed) = roll seed
  # (d3, seed) = roll seed
  # (d4, seed) = roll seed
  # (d5, seed) = roll seed
  = (isLargeStraight [d1, d2, d3, d4, d5], seed)

Start = 0
